import {Component, OnInit} from '@angular/core';
import {ResultDialogComponent} from "../result-dialog/result-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {Validators} from "@angular/forms";
import {FormCodeService} from "../../services/form-code.service";

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.scss']
})
export class TemplateDrivenFormComponent implements OnInit {

  public readonly componentName:string = "TemplateDrivenFormComponent";

  public user: any;

  public form1Code:string = "";
  public form2Code:string = "";
  public form3Code:string = "";
  public form4Code:string = "";

  public panelExpandStatus:boolean[] = [true, false, false, false]; // initially, 1st panel is expanded and others are collapsed.

  public usernameValidators = [Validators.required]; // validators for form4 username
  public passwordValidators = [Validators.required, Validators.minLength(8)]; // validators for form4 password

  constructor(public dialog: MatDialog,
              private formCodeService: FormCodeService) {
  }

  ngOnInit(): void {
    this.user = {
      username: "dilanka",
      password: "test@123"
    }

    this.setForm1Code();
    this.setForm2Code();
    this.setForm3Code();
    this.setForm4Code();
  }

  public submitForm(user:any = null): void {
    if(user) this.user = user;

    let data: any[] = [
      {
        field: "Username",
        value: this.user.username
      },
      {
        field: "Password",
        value: this.user.password
      },
    ];

    this.dialog.open(ResultDialogComponent, {
      data: {result: data, dialogType: 'template-driven'},
      width:'450px',
      autoFocus: false
    });
  }

  public expandPanel(panelIndex:number, event:any): void{
    if(!event) return; // neglects collapse events

    for(let i=0; i<this.panelExpandStatus.length; i++){
      if(i != panelIndex) this.panelExpandStatus[i] = false; // collapses all other panels
    }
  }

  private setForm1Code(): void {
    this.form1Code = this.formCodeService.getTemplateDrivenForm1Code();
  }

  private setForm2Code(): void {
    this.form2Code = this.formCodeService.getTemplateDrivenForm2Code();
  }

  private setForm3Code(): void {
    this.form3Code = this.formCodeService.getTemplateDrivenForm3Code();
  }

  private setForm4Code(): void {
    this.form4Code = this.formCodeService.getTemplateDrivenForm4Code();
  }
}
