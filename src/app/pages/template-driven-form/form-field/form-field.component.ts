import {AfterViewInit, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {NgForm, NgModel} from "@angular/forms";

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements AfterViewInit {

  @Input() public form: NgForm | undefined;
  @Input() public fieldLabel: string = "";
  @Input() public fieldType: string = "text";
  @Input() public validators: any[] = [];

  @Input() public fieldValue: string = "";
  @Output() public fieldValueChange = new EventEmitter<string>();

  @ViewChild('field_value') fieldValueNgModel: NgModel | undefined;

  constructor() {
  }

  ngAfterViewInit(): void {
    this.setValidators();
    if (this.fieldValueNgModel instanceof NgModel) {
      this.form?.addControl(this.fieldValueNgModel); // adds this->NgModel to form
    }
  }

  public onFieldValueChange() {
    this.fieldValueChange.emit(this.fieldValue); // emits value change
  }

  public setValidators() {
    this.fieldValueNgModel?.control?.setValidators(this.validators); // sets validators to this->NgModel
  }

}
