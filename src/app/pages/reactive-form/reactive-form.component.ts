import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ResultDialogComponent} from "../result-dialog/result-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {FormCodeService} from "../../services/form-code.service";

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {

  public readonly componentName:string = "ReactiveFormComponent";

  // Common
  public panelExpandStatus: boolean[] = [true, false, false, false]; // initially, 1st panel is expanded and others are collapsed.
  public itemCountToolTip: string = "";
  public itemCodeToolTip: string = "";
  public itemNameToolTip: string = "";
  public quantityToolTip: string = "";
  public commentToolTip: string = "";

  // Form1
  public itemCode: FormControl = new FormControl('', Validators.required);
  public quantity: FormControl = new FormControl('', [Validators.required, Validators.min(1), Validators.max(1000)]);
  public comment: FormControl = new FormControl('');
  public form1Code: string = "";

  // Form2
  public form2: FormGroup = new FormGroup({
    itemCode: new FormControl('', Validators.required),
    quantity: new FormControl('', [Validators.required, Validators.min(1), Validators.max(1000)]),
    comment: new FormControl('')
  });
  public form2Code: string = "";

  // Form3
  public form3: FormGroup = this.formBuilder.group({
    item: this.formBuilder.group({
      itemCode: ['', Validators.required],
      itemName: ['', Validators.maxLength(10)],
    }),
    quantity: ['', [Validators.required, Validators.min(1), Validators.max(1000)]],
    comment: ['']
  });
  public form3Code: string = "";

  // Form4
  public form4: FormGroup = new FormGroup({
    itemCount: new FormControl(1, [Validators.required, Validators.min(1), Validators.max(5)]),
    items: new FormArray([
      new FormGroup({
        itemCode: new FormControl('', Validators.required),
        quantity: new FormControl('', [Validators.required, Validators.min(1), Validators.max(1000)]),
        comment: new FormControl('')
      })
    ]),
  });
  public form4Code: string = "";


  constructor(private dialog: MatDialog,
              private formBuilder: FormBuilder,
              private formCodeService: FormCodeService) {
  }

  ngOnInit(): void {
    this.initToolTips();

    this.setForm1Code();
    this.setForm2Code();
    this.setForm3Code();
    this.setForm4Code();
  }

  public expandPanel(panelIndex: number, event: any): void {
    if (!event) return; // neglects collapse events

    for (let i = 0; i < this.panelExpandStatus.length; i++) {
      if (i != panelIndex) this.panelExpandStatus[i] = false; // collapses all other panels
    }
  }

  public initToolTips(): void {
    this.itemCountToolTip = "* Item Count is a mandatory field" + "\n";
    this.itemCountToolTip += "* Item Count must be greater than 0 and less than or equal 5";

    this.itemCodeToolTip = "* Item Code is a mandatory field";

    this.itemNameToolTip = "* Item Name must have less than or equal 10 characters"

    this.quantityToolTip = "* Quantity is a mandatory field" + "\n";
    this.quantityToolTip += "* Quantity must be greater than 0 and less than or equal 1000";

    this.commentToolTip = "* Comment is Not Applicable when Quantity is less than 100" + "\n";
    this.commentToolTip += "* Comment is a mandatory field when Quantity is greater than or equal 500";
  }


  // ===================== Form1 ==============================

  public form1QuantityChanged(): void {
    if (this.quantity.value < 100) {
      this.comment.setValue('N/A');
      this.comment.disable();
    } else {
      if (this.comment.value == 'N/A') { // clears only if text is N/A
        this.comment.setValue('');
      }
      this.comment.enable();
    }

    if (this.quantity.value >= 500) { // updates comment field validators according to quantity
      this.comment.setValidators([Validators.required]);
    }
    else{
      this.comment.clearValidators();
    }
    this.comment.updateValueAndValidity();
  }

  public submitForm1(): void {
    let data: any[] = [
      {
        field: "Item Code",
        value: this.itemCode.value
      },
      {
        field: "Quantity",
        value: this.quantity.value
      },
      {
        field: "Comment",
        value: this.comment.value
      }
    ];

    this.dialog.open(ResultDialogComponent, {
      data: {result: data, dialogType: 'reactive'},
      width: '450px',
      autoFocus: false
    });
  }

  private setForm1Code(): void {
    this.form1Code = this.formCodeService.getReactiveForm1Code();
  }


  // ===================== Form2 ==============================

  public form2QuantityChanged(): void {
    if (this.form2.get('quantity')?.value < 100) {
      this.form2.get('comment')?.patchValue('N/A');
      this.form2.get('comment')?.disable();
    }
    else {
      if (this.form2.get('comment')?.value == 'N/A') {
        this.form2.get('comment')?.patchValue(''); // clears only if text is N/A
      }
      this.form2.get('comment')?.enable();
    }

    if (this.form2.get('quantity')?.value >= 500) { // updates comment field validators according to quantity
      this.form2.get('comment')?.setValidators([Validators.required]);
    }
    else{
      this.form2.get('comment')?.clearValidators();
    }
    this.form2.get('comment')?.updateValueAndValidity();
  }

  public submitForm2(): void {
    let values: any = this.form2.getRawValue();
    let data = [
      {
        field: "Item Code",
        value: values['itemCode'] ? values['itemCode'] : ""
      },
      {
        field: "Quantity",
        value: values['quantity'] ? values['quantity'] : ""
      },
      {
        field: "Comment",
        value: values['comment'] ? values['comment'] : ""
      }
    ];

    this.dialog.open(ResultDialogComponent, {
      data: {result: data, dialogType: 'reactive'},
      width: '450px',
      autoFocus: false
    });
  }

  private setForm2Code(): void {
    this.form2Code = this.formCodeService.getReactiveForm2Code();
  }


  // ===================== Form3 ==============================

  public form3QuantityChanged(): void {
    if (this.form3.get('quantity')?.value < 100) {
      this.form3.get('comment')?.patchValue('N/A');
      this.form3.get('comment')?.disable();
    }
    else {
      if (this.form3.get('comment')?.value == 'N/A') {
        this.form3.get('comment')?.patchValue(''); // clears only if text is N/A
      }
      this.form3.get('comment')?.enable();
    }

    if (this.form3.get('quantity')?.value >= 500) { // updates comment field validators according to quantity
      this.form3.get('comment')?.setValidators([Validators.required]);
    }
    else{
      this.form3.get('comment')?.clearValidators();
    }
    this.form3.get('comment')?.updateValueAndValidity();
  }

  public submitForm3(): void {
    let values: any = this.form3.getRawValue();
    let data = [
      {
        field: "Item Code",
        value: values['item'] && values['item']['itemCode'] ? values['item']['itemCode'] : ""
      },
      {
        field: "Item Name",
        value: values['item'] && values['item']['itemName'] ? values['item']['itemName'] : ""
      },
      {
        field: "Quantity",
        value: values['quantity'] ? values['quantity'] : ""
      },
      {
        field: "Comment",
        value: values['comment'] ? values['comment'] : ""
      }
    ];

    this.dialog.open(ResultDialogComponent, {
      data: {result: data, dialogType: 'reactive'},
      width: '450px',
      autoFocus: false
    });
  }

  private setForm3Code(): void {
    this.form3Code = this.formCodeService.getReactiveForm3Code();
  }


  // ===================== Form4 ==============================

  public form4QuantityChanged(index: number): void {
    if (this.getItemFormGroup(index)?.get('quantity')?.value < 100) {
      this.getItemFormGroup(index)?.get('comment')?.patchValue('N/A');
      this.getItemFormGroup(index)?.get('comment')?.disable();
    }
    else {
      if (this.getItemFormGroup(index)?.get('comment')?.value == 'N/A') {
        this.getItemFormGroup(index)?.get('comment')?.patchValue(''); // clears only if text is N/A
      }
      this.getItemFormGroup(index)?.get('comment')?.enable();
    }

    if (this.getItemFormGroup(index)?.get('quantity')?.value >= 500) { // updates comment field validators according to quantity
      this.getItemFormGroup(index)?.get('comment')?.setValidators([Validators.required]);
    }
    else{
      this.getItemFormGroup(index)?.get('comment')?.clearValidators();
    }
    this.getItemFormGroup(index)?.get('comment')?.updateValueAndValidity();
  }

  public submitForm4(): void {
    let result: any[] = [];

    let itemCount: number = this.form4.get('itemCount')?.value;
    if (!itemCount) return;

    result.push({
      field: "Item Count",
      value: itemCount
    });

    for (let i = 0; i < itemCount; i++) {
      let group: FormGroup = this.getItemFormGroup(i);
      if (!group) continue;

      let values: any = group.getRawValue();
      result.push({
        field: 'Item-' + (i + 1) + ' Code',
        value: values['itemCode'] ? values['itemCode'] : ""
      });

      result.push({
        field: 'Item-' + (i + 1) + ' Quantity',
        value: values['quantity'] ? values['quantity'] : ""
      });

      result.push({
        field: 'Item-' + (i + 1) + ' Comment',
        value: values['comment'] ? values['comment'] : ""
      });
    }

    this.dialog.open(ResultDialogComponent, {
      data: {result: result, dialogType: 'reactive'},
      width: '450px',
      autoFocus: false
    });
  }

  public refreshItemList(): void {
    let newItemCount: number = this.form4.get('itemCount')?.value;
    let currentItemCount: number = (this.form4.get('items') as FormArray)?.length;
    if (!newItemCount || !currentItemCount) return;

    if (newItemCount > currentItemCount) {
      let itemsToAdd: number = newItemCount - currentItemCount;

      for (let i = 0; i < itemsToAdd; i++) {
        (this.form4.get('items') as FormArray)?.push( // adds new form groups according to item count and current item length
            new FormGroup({
              itemCode: new FormControl('', Validators.required),
              quantity: new FormControl('', [Validators.required, Validators.min(1), Validators.max(1000)]),
              comment: new FormControl('')
            })
        )
      }
    }
    else {
      for (let i = currentItemCount - 1; i >= newItemCount; i--) {
        (this.form4.get('items') as FormArray)?.removeAt(i);  // removes form groups from end according to item count and current item length
      }
    }
  }

  public getItemFormGroup(index: number): FormGroup {
    return (this.form4.get('items') as FormArray)?.at(index) as FormGroup; // returns form group from given index of form array
  }

  public get items(): FormArray {
    let array =  (this.form4.get('items') as FormArray); // returns form array
    return array ? array : new FormArray([]);
  }

  private setForm4Code(): void {
    this.form4Code = this.formCodeService.getReactiveForm4Code();
  }
}
