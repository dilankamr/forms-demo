import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
    selector: 'app-result-dialog',
    templateUrl: './result-dialog.component.html',
    styleUrls: ['./result-dialog.component.scss']
})
export class ResultDialogComponent implements OnInit {

    public result: any;
    public dialogType: string = "";
    public buttonColor: string = "";

    constructor(public dialogRef: MatDialogRef<ResultDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit(): void {
        this.result = this.data['result'];
        this.dialogType = this.data['dialogType'];
        this.buttonColor = this.dialogType == 'template-driven' ? 'primary' : 'accent'; // changes button colors according to previous page
    }

}
