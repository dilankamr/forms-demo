import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {TemplateDrivenFormComponent} from "./pages/template-driven-form/template-driven-form.component";
import {ReactiveFormComponent} from "./pages/reactive-form/reactive-form.component";
import {AboutMeComponent} from "./pages/about-me/about-me.component";

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'template-driven-form',
        component: TemplateDrivenFormComponent
    },
    {
        path: 'reactive-form',
        component: ReactiveFormComponent
    },
    {
        path: 'about-me',
        component: AboutMeComponent
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
