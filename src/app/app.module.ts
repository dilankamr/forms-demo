import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { TemplateDrivenFormComponent } from './pages/template-driven-form/template-driven-form.component';
import { ReactiveFormComponent } from './pages/reactive-form/reactive-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule} from "@angular/material/dialog";
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ResultDialogComponent } from './pages/result-dialog/result-dialog.component';
import { MatIconModule} from "@angular/material/icon";
import { FormFieldComponent } from './pages/template-driven-form/form-field/form-field.component';
import { MatTooltipModule} from "@angular/material/tooltip";
import { AboutMeComponent } from './pages/about-me/about-me.component';
import { PdfViewerModule } from "ng2-pdf-viewer";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TemplateDrivenFormComponent,
    ReactiveFormComponent,
    ResultDialogComponent,
    FormFieldComponent,
    AboutMeComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatDialogModule,
        MatExpansionModule,
        MatTooltipModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        PdfViewerModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
