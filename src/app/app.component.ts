import { Component } from '@angular/core';
import {ReactiveFormComponent} from "./pages/reactive-form/reactive-form.component";
import {TemplateDrivenFormComponent} from "./pages/template-driven-form/template-driven-form.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'templates-demo';

  public showReactiveFormsBtn:boolean = false;
  public showTemplateDrivenFormsBtn:boolean = false;
  public showHomeBtn:boolean = false;
  public showAboutMeBtn:boolean = false;

  constructor() {

  }
  public appRouteChanged(component:any){
    if(component && component['componentName'] && component['componentName'] == "TemplateDrivenFormComponent"){
      this.showReactiveFormsBtn = true;
      this.showTemplateDrivenFormsBtn = false;
      this.showHomeBtn = true;
      this.showAboutMeBtn = true;
    }
    else if(component && component['componentName'] && component['componentName'] == "ReactiveFormComponent"){
      this.showReactiveFormsBtn = false;
      this.showTemplateDrivenFormsBtn = true;
      this.showHomeBtn = true;
      this.showAboutMeBtn = true;
    }
    else if(component && component['componentName'] && component['componentName'] == "AboutMeComponent"){
      this.showReactiveFormsBtn = true;
      this.showTemplateDrivenFormsBtn = true;
      this.showHomeBtn = true;
      this.showAboutMeBtn = false;
    }
    else {
      this.showReactiveFormsBtn = false;
      this.showTemplateDrivenFormsBtn = false;
      this.showHomeBtn = false;
      this.showAboutMeBtn = true;
    }
  }
}
